version: "3.3"

networks:
    nuxnet:

services:

  nginx:
    build: nginx
    ports:
      # For localhost use, the exposed nginx port
      # must match the localhost:port below in NUXEO_URL
      - "8080:80"
      #- "443:443"
    cap_add:
      - NET_BIND_SERVICE
    links:
      - nuxeo1
    #  - nuxeo2
    environment:
      USE_STAGING: 1
      # default is 4096, but gcloud requires 2048
      KEYSIZE: 2048
      DOMAIN_LIST: /etc/nginx/conf.d/domains.txt
    devices:
      - "/dev/urandom:/dev/random"
    sysctls:
      - net.core.somaxconn=511
    volumes:
      - ./nginx/conf.d:/etc/nginx/conf.d:ro
      - certs:/etc/ssl/acme
    networks:
      - nuxnet
    restart: always

  nuxeo1:
    image: nuxeo:ubuntu-9.2
    hostname: nuxeo1
    links:
      - redis
      - es
      - db
    env_file:
      - ./nuxeo/setup.env
    environment:
      # Each nuxeo container must have a unique cluster id
      NUXEO_CLUSTER_ID: 1

      # URL that a user would use to access nuxeo UI or API
      # For localhost urls, the port must match the exposted nginx port above
      NUXEO_URL: http://localhost:8080/nuxeo

      # JAVA memory tuning -Xms, -Xmx
      JVM_MS: 1024m
      JVM_MX: 2048m
    devices:
      - "/dev/urandom:/dev/random"
    volumes:
      - ./nuxeo/init:/docker-entrypoint-initnuxeo.d:ro
      - app-data:/var/lib/nuxeo
      - app-logs:/var/log/nuxeo
    networks:
      - nuxnet
    restart: always

  redis:
    # note: based on alpine:3.6
    # see https://hub.docker.com/_/redis/
    image: redis:3.2-alpine
    volumes:
      - redis-data:/data
    networks:
      - nuxnet
    restart: always

  es:
    image: elasticsearch:2.4-alpine
    volumes:
      - es-data:/usr/share/elasticsearch/data
      - es-plugins:/usr/share/elasticsearch/plugins
      - es-config:/usr/share/elasticsearch/config
      - ./elasticsearch/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml
    environment:
      # settings below add -Xms400m -Xmx1g
      ES_MIN_MEM: 500m
      EX_MAX_MEM: 1g
    security_opt:
      - seccomp:unconfined
    networks:
      - nuxnet
    restart: always

  db:
    image: postgres:9.6-alpine
    # note mem tuning suggestions in the following two links
    # https://doc.nuxeo.com/nxdoc/postgresql/
    # https://doc.nuxeo.com/nxdoc/postgresql/#adapt-your-configuration-to-your-hardware
    environment:
       POSTGRES_USER: nuxeo
       POSTGRES_PASSWORD: nuxeo
       POSTGRES_DB: nuxeo
       POSTGRES_INITDB_ARGS: "-E UTF8"
       PGDATA: /var/lib/postgresql/data
    volumes:
      - db-store:/var/lib/postgresql/data
      - ./postgresql/postgresql.conf:/etc/postgresql.conf:ro
    command: postgres -c config_file=/etc/postgresql.conf
    networks:
      - nuxnet
    restart: always

volumes:
  # to view current data, run bin/view-data.sh
  certs:
  app-logs:   # all server logs, can be shared between instances
  app-data:   # contains app data and packages (store cache), can be shared between instances
  db-store:   # postgres database
  es-data:
  es-plugins:
  es-config:
  redis-data:
