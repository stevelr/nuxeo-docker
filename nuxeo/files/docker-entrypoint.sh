#!/bin/bash 
set -e

# The environment is initialized by docker-compose with the contents of setup.env
# This script runs as root to fixup permissions, then does the rest as user nuxeo

export NUXEO_LOG=/var/log/nuxeo/$NUXEO_CLUSTER_ID
export NUXEO_DATA=/var/lib/nuxeo/data/$NUXEO_CLUSTER_ID
export NUXEO_MPDIR=/var/lib/nuxeo/packages/$NUXEO_CLUSTER_ID

# ensure log dir is writable
mkdir -p $NUXEO_LOG $NUXEO_DATA $NUXEO_MPDIR \
    && chown -R ${NUXEO_USER_ID}:0 $NUXEO_LOG $NUXEO_DATA $NUXEO_MPDIR \
    && chmod -R g+rwX              $NUXEO_LOG $NUXEO_DATA $NUXEO_MPDIR

#export PATH=$NUXEO_HOME/bin:$PATH

# generate nuxeo.conf from template
TEMPLATE_PROCESSOR="import os,sys,jinja2; jinja2.Template(sys.stdin.read()).stream(dict(os.environ)).dump(sys.stdout)"
python3 -c "$TEMPLATE_PROCESSOR" < /etc/nuxeo/nuxeo.conf.jtemplate > $NUXEO_CONF
if [ -f /docker-entrypoint-initnuxeo.d/nuxeo.jconf ]; then
    python3 -c "$TEMPLATE_PROCESSOR" < /docker-entrypoint-initnuxeo.d/nuxeo.jconf >> $NUXEO_CONF
fi
chown ${NUXEO_USER_ID}:0 $NUXEO_CONF

if [ -f $NUXEO_HOME/FIRSTBOOT ]; then
    echo First-time initialization of this image...
    $NUXEO_HOME/bin/nuxeoctl mp-init
    rm -f $NUXEO_HOME/FIRSTBOOT
fi

# Protecting writeable directories to support arbitrary User IDs for OpenShift
# https://docs.openshift.com/container-platform/3.4/creating_images/guidelines.html
chown -R ${NUXEO_USER_ID}:0 /etc/nuxeo $NUXEO_HOME $NUXEO_LOG $NUXEO_DATA $NUXEO_MPDIR 
chmod -R g+rwX              /etc/nuxeo $NUXEO_HOME $NUXEO_LOG $NUXEO_DATA $NUXEO_MPDIR 
mkdir -p /var/run/nuxeo 
chown -R ${NUXEO_USER_ID}:0 /var/run/nuxeo  && chmod -R g+rwX /var/run/nuxeo 
mkdir -p /docker-entrypoint-initnuxeo.d 
chown -R ${NUXEO_USER_ID}:0 /docker-entrypoint-initnuxeo.d && chmod -R g+rwX /docker-entrypoint-initnuxeo.d

export PATH=$NUXEO_HOME/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$JAVA_HOME/jre/bin:$JAVA_HOME/bin

# process the next file with uid $NUXEO_USER
exec /usr/local/bin/gosu ${NUXEO_USER} /docker-init.sh "$@"
