##=============================================================================
## Nuxeo configuration file for Docker
##=============================================================================
## See https://doc.nuxeo.com/nxdoc/configuration-parameters-index-nuxeoconf/
## for detailed information about the settings below.

## This file is a jinja2 template processed by python3 before tomcat-nuxeo server starts.
## The template context includes all environment variables in the global namespace.
## To insert the value of environment variable VAR, use 
##         {{ VAR }}
## To insert value of a variable VAR if defined, or "DVAL" if VAR is undefined, use
##         {{ VAR |default("DVAL") }}
## See jinja2 documentation for documentation if you need to add branch logic
## or other fancier templating features

##-----------------------------------------------------------------------------
## Java
##-----------------------------------------------------------------------------

## Heap size
JAVA_OPTS=-Xms1024m -Xmx2048m

## File encoding and encoded MIME parameters support
JAVA_OPTS=$JAVA_OPTS -Dfile.encoding=UTF-8 -Dmail.mime.decodeparameters=true

## Update sort behavior for Arrays and Collections may throw an IllegalArgumentException
JAVA_OPTS=$JAVA_OPTS -Djava.util.Arrays.useLegacyMergeSort=true

## Log Garbage Collector informations into a file
JAVA_OPTS=$JAVA_OPTS -Xloggc:"${nuxeo.log.dir}/gc.log" -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps

JAVA_OPTS=$JAVA_OPTS -Dserver.hostname={{HOSTNAME}} -Dserver.cluster.id={{NUXEO_CLUSTER_ID}}

##-----------------------------------------------------------------------------
## Nuxeo
##-----------------------------------------------------------------------------

nuxeo.force.generation=true
nuxeo.templates=common,postgresql

nuxeo.db.type=postgresql
nuxeo.db.host={{ NUXEO_DB_HOST |default('db') }}
nuxeo.db.name={{ NUXEO_DB_NAME |default('nuxeo') }}
nuxeo.db.user={{ NUXEO_DB_USER |default('nuxeo') }}
nuxeo.db.password={{ NUXEO_DB_PASSWORD |default('nuxeo') }}
#nuxeo.db.validationQuery="SELECT 1;"

nuxeo.url={{ NUXEO_URL }}

# security: don't disclose server version
nuxeo.server.signature="Server"

elasticsearch.networkHost=
# comma-separated list of elasticsearch servers
elasticsearch.addressList=es:9300
# clusterName should match value in elasticsearch.yml
# (see nuxeo/files/elasticsearch.yml and elasticsearch/elasticsearch.yml)
elasticsearch.clusterName=elasticsearch
elasticsearch.indexName=nuxeo
elasticsearch.indexNumberOfReplicas=0
audit.elasticsearch.enabled=true
audit.elasticsearch.indexName=nuxeo-audit
seqgen.elasticsearch.indexName=nuxeo-uidgen

# redis
nuxeo.redis.enabled=true
nuxeo.redis.host={{ NUXEO_REDIS_HOST |default('redis') }}
nuxeo.redis.port={{ NUXEO_REDIS_PORT |default(6379) }}

#org.nuxeo.automation.trace={{ NUXEO_AUTOMATION_TRACE }}
org.nuxeo.dev={{ NUXEO_DEV_MODE }}

nuxeo.vcs.ddlmode={{ NUXEO_DDL_MODE }}

## CLUSTERING
repository.clustering.enabled=true
repository.clustering.id={{ NUXEO_CLUSTER_ID }}
# delay (ms) during which invalidations don't need to be processed
repository.clustering.delay=2000
repository.binary.store={{ NUXEO_BINARY_STORE }}

nuxeo.wizard.done=true

#nuxeo.transientstore.default.path={{ NUXEO_TRANSIENT_STORE }}

server.status.key={{ SERVER_STATUS_KEY }}
server.crypt.keystore.pass={{ SERVER_KEYSTORE_KEY }}

nuxeo.pid.dir=/var/run/nuxeo

# Logging dir (set in environment by /docker-entrypoint.sh _before_ this is evaluated)
nuxeo.log.dir={{ NUXEO_LOG }}

# Data directory
nuxeo.data.dir={{ NUXEO_DATA }}

# Package installation
nuxeo.mp.dir={{ NUXEO_MPDIR }}


# Custom parameters in key=value format
{{ NUXEO_CUSTOM_PARAM|default('') }}

