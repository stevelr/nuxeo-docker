#!/bin/sh

# this script should run weekly in /etc/periodic/weekly

if [ -n "$USE_STAGING" ]; then
    STAGING_FLAG="-s"
fi

KEYSIZE=${KEYSIZE:-4096}

mkdir -p /etc/ssl/acme/account /etc/ssl/acme/private
# on first run, get rid of /etc/acme and link to /etc/ssl/account so account key will be in the docker-volume
[ -f /etc/acme/UNLINK ] && rm -rf /etc/acme && ln -s /etc/ssl/acme/account /etc/acme

renew=0

cat "${DOMAIN_LIST}" | while read domain line ; do
   
   if [ $(echo $domain | head -c 1) = '#' ]; then
       echo "Skipping comment: $domain"
   elif [ "$domain" = "127.0.0.1" ]; then
       echo "Skipping localhost: $domain"
   else
       # acme_client always generates 4096 bit keys, but will use a domain key of different size
       # if it's created already. First time, generate key of the requested size
       if [ ! -f /etc/ssl/acme/private/${domain}/privkey.pem ]; then
         mkdir -p /etc/ssl/acme/private/${domain}
         openssl genrsa -out /etc/ssl/acme/private/${domain}/privkey.pem $KEYSIZE
       fi
       set +e # RC=2 when time to expire > 30 days
       acme-client -v -e -m -b -n ${STAGING_FLAG} ${domain} ${line}
       RC=$?
       set -e
       [ $RC -ne 0 -a $RC -ne 2 ] && exit $RC
       [ $RC -eq 0 ] && renew=1
   fi
done

[ "$renew" = 1 ] && nginx -s reload
